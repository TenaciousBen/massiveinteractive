﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Massive.Core.Exceptions
{
    public class DeserializationException : Exception
    {
        public DeserializationException(node node)
            : base($"Failed to deserialize node due to a type conversion error or missing id or label. Node info: id - {node.id} label - {node.label} - adjacent - {node.adjacentNodes?.ToCsv(n => n.Value) ?? "(null)"}")
        {
        }
    }
}
