﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Massive.Core.DAL;

namespace Massive.Tests.Mocks
{
    public class TestVertexRepository : VertexRepository
    {
        public TestVertexRepository()
        {
            ChangeDatabaseFile("massivetest");
        }
    }
}
