﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Massive.Importer
{
    public static class ImporterExtensions
    {
        public static bool? ToBooleanFromYesNo(this string str)
        {
            if (string.IsNullOrWhiteSpace(str)) return null;
            if (str.ToLower() == "y" || str.ToLower() == "yes") return true;
            else if (str.ToLower() == "n" || str.ToLower() == "no") return false;
            return null;
        }
    }
}
