﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Massive.Wpf.Views
{
    /// <summary>
    /// A connector between two points on a canvas.
    /// Adapted from: http://stackoverflow.com/questions/2823266/how-to-draw-connecting-lines-between-two-controls-on-a-grid-wpf
    /// </summary>
    public partial class Connector : UserControl
    {
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register(
                "Source", typeof(Point), typeof(Connector),
                new FrameworkPropertyMetadata(default(Point)));

        public Point Source
        {
            get { return (Point)this.GetValue(SourceProperty); }
            set { this.SetValue(SourceProperty, value); }
        }

        public static readonly DependencyProperty DestinationProperty =
            DependencyProperty.Register(
                "Destination", typeof(Point), typeof(Connector),
                    new FrameworkPropertyMetadata(default(Point)));

        public Point Destination
        {
            get { return (Point)this.GetValue(DestinationProperty); }
            set { this.SetValue(DestinationProperty, value); }
        }

        public Connector(bool pathed = false)
        {
            var segment = new LineSegment(default(Point), true);
            var figure = new PathFigure(default(Point), new[] { segment }, false);
            var geometry = new PathGeometry(new[] { figure });
            var sourceBinding = new Binding { Source = this, Path = new PropertyPath(SourceProperty) };
            var destinationBinding = new Binding { Source = this, Path = new PropertyPath(DestinationProperty) };
            BindingOperations.SetBinding(figure, PathFigure.StartPointProperty, sourceBinding);
            BindingOperations.SetBinding(segment, LineSegment.PointProperty, destinationBinding);
            Content = new Path
            {
                Data = geometry,
                StrokeThickness = 5,
                //when pathed, highlight the connector so that vertices it travels under don't appear part of the path
                Stroke = pathed ? Brushes.Aqua : Brushes.Black,
                MinWidth = 1,
                MinHeight = 1
            };
            //when pathed, make the z-index higher than other connectors, so a pathed connector doesn't appear underneath an unpathed connector
            Panel.SetZIndex(this, pathed ? 1 : 0);
        }
    }
}
