﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Threading.Tasks;
using Massive.Core;
using Massive.Wpf;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Massive.Tests
{
    [TestClass]
    public class UITests : TestBase
    {
        [TestMethod]
        public void CanTriggerBasicCommand()
        {
            bool triggered = false;
            var command = new BasicCommand(() => triggered = true);
            Assert.IsTrue(command.CanExecute(null));
            command.Execute(null);
            Assert.IsTrue(triggered);
        }

        [TestMethod]
        public void CanTriggerValidatedCommand()
        {
            bool triggered = false;
            var command = new ValidatedCommand<bool>(() => triggered = true, (param) => param);
            Assert.IsFalse(command.CanExecute(false));
            Assert.IsTrue(command.CanExecute(true));
            command.Execute(null);
            Assert.IsTrue(triggered);
        }

        [TestMethod]
        public void CanCoerceCommandValue()
        {
            bool triggered = false;
            var command = new ValidatedCommand<float>(() => triggered = true, float.IsNegativeInfinity);
            Assert.IsFalse(command.CanExecute(12f));
            Assert.IsTrue(command.CanExecute(float.NegativeInfinity));
            command.Execute(null);
            Assert.IsTrue(triggered);
        }

        [TestMethod]
        public void InvalidCoercionThrowsInCommand()
        {
            Exception thrown = null;
            try
            {
                var command = new BasicCommand<string>((s) => {});
                command.Execute(123);
            }
            catch (Exception e)
            {
                thrown = e;
            }
            Assert.IsNotNull(thrown);
        }

        [TestMethod]
        public async Task ChangingVerticeCausesGraphToNotify()
        {
            var triggered = false;
            var vertices = new List<Vertex> {new Vertex()};
            var graph = new Graph(vertices);
            graph.PropertyChanged += (sender, e) => triggered = true;
            Assert.IsFalse(triggered);
            vertices[0].IsSelected = true;
            await AwaitableCondition(() => triggered, timeoutMs: 100);
            Assert.IsTrue(triggered);
        }
    }
}
