﻿using System;
using System.Collections.Generic;
using System.Linq;
using Massive.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Massive.Tests
{
    /// <summary>
    /// Tests the graph and pathing functionality. Diagrams in comments use 0 for vertices, s for the start vertex,
    /// f for the finish vertex and - or | for edges.
    /// </summary>
    [TestClass]
    public class GraphTests : TestBase
    {
        /// <summary>
        /// s - 0 - f
        ///   - 0
        /// </summary>
        [TestMethod]
        public void CanPathVerticesWithSinglePath()
        {
            var start = CreateVertex();
            var deadEnd = CreateVertex(start);
            var middle = CreateVertex(start);
            var finish = CreateVertex(middle);
            var graph = new Graph(new List<Vertex> { start, deadEnd, middle, finish });
            var shortestPath = graph.ShortestPath(start, finish);
            Assert.IsTrue(shortestPath.PathsTo(finish));
            Assert.AreEqual(3, shortestPath.Distance);
            Assert.IsTrue(shortestPath.Vertices.SequenceEqual(new List<Vertex> { start, middle, finish }));
        }

        /// <summary>
        /// s - 0 - f - |
        ///   - 0 - 0 - |
        /// </summary>
        [TestMethod]
        public void CanPathVerticesWithMultiplePaths()
        {
            var start = CreateVertex();
            var middle = CreateVertex(start);
            var finish = CreateVertex(middle);
            var longerStart = CreateVertex(start);
            var longerMiddle = CreateVertex(longerStart);
            Edge(longerMiddle, finish);
            var graph = new Graph(new List<Vertex> { start, middle, finish, longerStart, longerMiddle });
            var shortestPath = graph.ShortestPath(start, finish);
            Assert.IsTrue(shortestPath.PathsTo(finish));
            Assert.AreEqual(3, shortestPath.Distance);
            Assert.IsTrue(shortestPath.Vertices.SequenceEqual(new List<Vertex> { start, middle, finish }));

        }

        /// <summary>
        /// Tests that the pathing avoids getting trapped in recursion
        /// s - 0 - f
        ///   - 0 -
        ///     |_|
        /// </summary>
        [TestMethod]
        public void DoesAvoidCycling()
        {
            var start = CreateVertex();
            var deadEnd = CreateVertex(start);
            var middle = CreateVertex(start);
            var finish = CreateVertex(middle);
            Edge(deadEnd, deadEnd);
            var graph = new Graph(new List<Vertex> { start, deadEnd, middle, finish });
            var shortestPath = graph.ShortestPath(start, finish);
            Assert.IsTrue(shortestPath.PathsTo(finish));
            Assert.AreEqual(3, shortestPath.Distance);
            Assert.IsTrue(shortestPath.Vertices.SequenceEqual(new List<Vertex> { start, middle, finish }));
        }

        /// <summary>
        /// Tests that pathing between two vertices which share no edges returns null rather than erroring out
        /// s - 0 - 0
        /// f
        /// </summary>
        [TestMethod]
        public void InvalidPathReturnsNull()
        {
            var start = CreateVertex();
            var middle = CreateVertex(start);
            var finish = CreateVertex(middle);
            var orphaned = CreateVertex();
            var graph = new Graph(new List<Vertex> { start, middle, finish, orphaned });
            var shortestPath = graph.ShortestPath(start, orphaned);
            Assert.IsNull(shortestPath);
        }
    }
}
