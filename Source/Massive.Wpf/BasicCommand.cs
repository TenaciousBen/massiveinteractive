﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Massive.Wpf
{
    public abstract class BaseCommand : ICommand
    {
        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        protected TCoerced TryCoerce<TCoerced>(object instance)
        {
            if (instance is TCoerced || instance == (object)default(TCoerced)) return (TCoerced)instance;
            throw new Exception($"Command failed. Expected type {typeof(TCoerced)} got {instance?.GetType()?.Name ?? "(null)"}.");
        }

        public abstract bool CanExecute(object parameter);

        public abstract void Execute(object parameter);

        public event EventHandler CanExecuteChanged;
    }

    public class BasicCommand<T> : BaseCommand
    {
        public Action<T> Action { get; set; }

        public BasicCommand(Action<T> action)
        {
            Action = action;
        }

        public override bool CanExecute(object parameter) => true;

        public override void Execute(object parameter)
        {
            Action(TryCoerce<T>(parameter));
        }
    }
    public class BasicCommand : BaseCommand
    {
        public Action Action { get; set; }

        public BasicCommand(Action action)
        {
            Action = action;
        }

        public override bool CanExecute(object parameter) => true;

        public override void Execute(object parameter)
        {
            Action();
        }
    }

    public class ValidatedCommand<TAction, TValidation> : BasicCommand<TAction>
    {
        public Func<TValidation, bool> Validation { get; set; }

        public ValidatedCommand(Action<TAction> action, Func<TValidation, bool> validation) : base(action)
        {
            Validation = validation;
        }

        public override bool CanExecute(object parameter)
        {
            return Validation(TryCoerce<TValidation>(parameter));
        }
    }

    public class ValidatedCommand<TValidation> : BasicCommand
    {
        public Func<TValidation, bool> Validation { get; set; }

        public ValidatedCommand(Action action, Func<TValidation, bool> validation) : base(action)
        {
            Validation = validation;
        }

        public override bool CanExecute(object parameter)
        {
            return Validation(TryCoerce<TValidation>(parameter));
        }
    }
}
