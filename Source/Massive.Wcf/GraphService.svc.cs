﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Massive.Core;
using Massive.Core.DAL;

namespace Massive.Wcf
{
    public class GraphService : IGraphService
    {
        public VertexRepository VertexRepository { get; set; }

        public GraphService(VertexRepository repository)
        {
            VertexRepository = repository;
        }

        public GraphService() : this (new VertexRepository())
        {
        }

        public Graph GetGraph()
        {
            var vertices = VertexRepository.ReadVertices();
            var graph = new Graph(vertices);
            return graph;
        }

        public List<Vertex> GetShortestPath(PathRequest request)
        {
            var graph = GetGraph();
            //find the vertices in the request in the above graph of all vertices
            var startVertex = GetVertexById(graph, request.StartId);
            var endVertex = GetVertexById(graph, request.EndId);
            var path = graph.ShortestPath(startVertex, endVertex);
            return path?.Vertices ?? new List<Vertex>();
        }

        private Vertex GetVertexById(Graph graph, int id)
        {
            var vertex = graph.GetVertexById(id);
            if (vertex == null) throw new Exception($"Cannot find vertex with id {id} in the graph");
            return vertex;
        }
    }
}
