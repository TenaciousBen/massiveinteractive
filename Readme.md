## Required tools ##

Visual Studio 2015 is required to run the solution, as it makes extensive use of C#6. Please ensure that Visual Studio 2015 is running as an administrator, as the database needs to create a .db file to store its data.

## Configuring the solution ##

The solution should be configured to run the console app, WCF app and WPF app simultaneously when run from within Visual Studio. The SQLite database is wrapped in a DAL such that running the import in the console will automatically make the imported data available to the WCF and WPF projects.

## Usage ##

The WPF frontend works as follows:

* The graph can be loaded from the WCF backend by pressing the 'Load graph' button
* Vertices are selected or deselected by clicking them
* A selected vertice has a white background, an unselected vertice has a black background
* When two vertices are selected, the 'Calculate shortest path' button becomes available
* A calculated path displays on the WPF form by highlighting the vertices and their connecting edges in blue

Note that vertices are arranged in a simple grid in ascending order of their number of edges, so edges may run underneath vertices without connecting to them.