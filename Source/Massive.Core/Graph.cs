﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Massive.Core.Annotations;

namespace Massive.Core
{
    /// <summary>
    /// Represents a graph of vertices, providing methods to path between vertices and notify
    /// subscribers of changes to the vertices.
    /// </summary>
    [DataContract]
    public class Graph : INotifyPropertyChanged
    {
        [DataMember]
        public List<Vertex> Vertices { get; set; }

        public Graph(List<Vertex> vertices)
        {
            Vertices = vertices;
            StartWatchingVertices();
        }

        public Graph()
        {
            Vertices = new List<Vertex>();
        }

        /// <summary>
        /// Returns the shortest path between vertex a and b if one exists, else null.
        /// </summary>
        public Path ShortestPath(Vertex a, Vertex b, Path path = null)
        {
            //if no path exists, create one starting at the start vertex
            if (path == null) path = new Path(a);
            else
            {
                //if a path already exists, copy it so as not to mutate it and add the current vertex
                path = path.Copy();
                path.AddVertex(a);
            }
            //quit out of the recursion if we've reached the destination, or already travelled to all adjacents, or have no adjacents
            if (a == b || a.Adjacent.All(path.PathsTo) || !a.Adjacent.Any()) return path;
            //recursively path all adjacents we've not already travelled to, and return the one which reaches the destination and has the shortest distance
            return a.Adjacent.Where(adj => !path.PathsTo(adj))
                .Select(adj => ShortestPath(Vertices.First(v => v.Id == adj), b, path.Copy()))
                .Where(p => p != null && p.PathsTo(b))
                .MinBy(p => p.Distance);
        }

        /// <summary>
        /// Clears the 'Path' status from all vertices on the graph, and sets the 'Path' status of those
        /// vertices whose ids match the ids of the parameter vertices. Notifies subscribers of INotifyPropertyChanged
        /// that the vertices have changed.
        /// </summary>
        public void SetActivePath(List<Vertex> path)
        {
            foreach (var vertice in Vertices) vertice.Path = path.Any(i => i.Id == vertice.Id);
            NotifyVerticesChanged(this, EventArgs.Empty);
        }

        /// <summary>
        /// Subscribes the Graph to the INotifyPropertyChanged event of its vertices, so that the graph will notify
        /// its own subscribers if any of its vertices change
        /// </summary>
        public void StartWatchingVertices()
        {
            foreach (var vertice in Vertices)
            {
                //remove the subscription if it already exists so we don't end up doubly subscribed
                vertice.PropertyChanged -= NotifyVerticesChanged;
                vertice.PropertyChanged += NotifyVerticesChanged;
            }
        }

        /// <summary>
        /// Notifies all subscribers of INotifyPropertyChanged that the graph's vertices have changed.
        /// </summary>
        public void NotifyVerticesChanged(object sender, EventArgs args) => OnPropertyChanged(nameof(Vertices));

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Returns the vertex contained in the graph with the parameter id if one exists, else null.
        /// </summary>
        public Vertex GetVertexById(int id)
        {
            return Vertices.FirstOrDefault(v => v.Id == id);
        }
    }

    /// <summary>
    /// Represents a path between two vertices along the edges and vertices that connect them.
    /// </summary>
    public class Path
    {
        /// <summary>
        /// The list of vertices on the path, in the order of start to finish.
        /// </summary>
        public List<Vertex> Vertices { get; set; }
        /// <summary>
        /// The distance between the start vertice and end vertice measured in the number 
        /// of connecting vertices.
        /// </summary>
        public int Distance { get; set; }

        public Path(List<Vertex> vertices)
        {
            Vertices = vertices;
            Distance = Vertices.Count;
        }

        public Path(Vertex start) : this(new List<Vertex> { start })
        {
        }

        /// <summary>
        /// Adds the parameter vertex to the path and increments the distance counter. If the
        /// parameter vertex is already contained on the path, a DuplicateNameException is thrown.
        /// </summary>
        public void AddVertex(Vertex v)
        {
            if (PathsTo(v)) throw new DuplicateNameException($"Vertex {v.Id} - {v.Label} is already contained in the graph");
            Vertices.Add(v);
            Distance += 1;
        }

        /// <summary>
        /// Returns a copy of this path, with internal copies of all reference types.
        /// Allows manipulation of a path in isolation, such that e.g. two vertices with a 
        /// common ancestor can both copy the path to avoid writing to each other's paths.
        /// </summary>
        public Path Copy() => new Path(Vertices.ToList());

        /// <summary>
        /// Returns true if the parameter vertex is on the path.
        /// </summary>
        public bool PathsTo(Vertex v) => Vertices.Contains(v);

        /// <summary>
        /// Returns true if any vertex on the path has the parameter id.
        /// </summary>
        public bool PathsTo(int id) => Vertices.Any(v => v.Id == id);

        /// <summary>
        /// Returns the vertex at the start of the path.
        /// </summary>
        public Vertex Start() => Vertices.FirstOrDefault();

        /// <summary>
        /// Returns the vertex at the end of the path.
        /// </summary>
        public Vertex End() => Vertices.LastOrDefault();

        /// <summary>
        /// Lists the vertex ids in order from the start of the path to the end as a CSV.
        /// </summary>
        public override string ToString() => Vertices.ToCsv((i) => i.Id);
    }

    /// <summary>
    /// Represents a vertex, AKA node, on the graph.
    /// </summary>
    [DataContract]
    public class Vertex : INotifyPropertyChanged
    {
        [DataMember]
        private bool _isSelected;
        /// <summary>
        /// Identifier for the vertice.
        /// </summary>
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Label { get; set; }
        /// <summary>
        /// Lists the Ids of vertices with edges to or from this vertice. As this exercise uses
        /// an undirected graph, there is no significance to the direction of the edge.
        /// </summary>
        [DataMember]
        public List<int> Adjacent { get; set; }
        /// <summary>
        /// Signifies whether this vertice is part of the currently displayed path.
        /// </summary>
        [DataMember]
        public bool Path { get; set; }
        /// <summary>
        /// Signifies whether this vertice has been selected by the user. Notifies subscribers of
        /// INotifyPropertyChanged when changed.
        /// </summary>
        [DataMember]
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (value == _isSelected) return;
                _isSelected = value;
                OnPropertyChanged();
            }
        }

        public Vertex(int id, string label)
        {
            Id = id;
            Label = label;
            Adjacent = new List<int>();
        }

        public Vertex() : this(0, null)
        {
        }
        
        public override string ToString() => $"{Id} {Label} - IsSelected:{IsSelected} - Path:{Path}";

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
