﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Massive.Core;

namespace Massive.Tests
{
    public abstract class TestBase
    {
        public int VertexIdCounter { get; set; } = 1;

        protected async Task AwaitableCondition(Func<bool> result, int timeoutMs = -1)
        {
            await Task.Factory.StartNew(() =>
            {
                var timeout = 0;
                while (!result() && timeout < timeoutMs)
                {
                    Thread.Sleep(1);
                    timeout++;
                }
            });
        }

        protected Vertex CreateVertex(Vertex previous = null)
        {
            var v = new Vertex(VertexIdCounter, "TestVertex");
            VertexIdCounter++;
            if (previous != null)
            {
                previous.Adjacent.Add(v.Id);
                v.Adjacent.Add(previous.Id);
            }
            return v;
        }

        protected void Edge(Vertex a, Vertex b)
        {
            if (!a.Adjacent.Contains(b.Id)) a.Adjacent.Add(b.Id);
            if (!b.Adjacent.Contains(a.Id)) b.Adjacent.Add(a.Id);
        }
        protected bool VerticesAreSame(Vertex a, Vertex b) => a.Id == b.Id && a.Label == b.Label && a.Adjacent.SequenceEqual(b.Adjacent);
    }
}
