﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Massive.Core;
using Massive.Core.DAL;
using Massive.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Massive.Tests
{
    [TestClass]
    public class DalTests : TestBase
    {
        [TestMethod]
        public void CanReadAndWriteVertices()
        {
            var repo = new TestVertexRepository();
            repo.CreateTable();
            var start = CreateVertex();
            var deadEnd = CreateVertex(start);
            var middle = CreateVertex(start);
            var finish = CreateVertex(middle);
            repo.CreateVertex(start);
            repo.CreateVertex(deadEnd);
            repo.CreateVertex(middle);
            repo.CreateVertex(finish);
            var vertices = repo.ReadVertices();
            Assert.AreEqual(1, vertices.Count(a => VerticesAreSame(a, start)));
            Assert.AreEqual(1, vertices.Count(a => VerticesAreSame(a, deadEnd)));
            Assert.AreEqual(1, vertices.Count(a => VerticesAreSame(a, middle)));
            Assert.AreEqual(1, vertices.Count(a => VerticesAreSame(a, finish)));
        }

        [TestMethod]
        public void SqlInjectionIsUnsuccessful()
        {
            var repo = new TestVertexRepository();
            repo.CreateTable();
            var injectionNamed = CreateVertex();
            injectionNamed.Label = $"' DROP TABLE {repo.TableName}";
            Exception thrown = null;
            try
            {
                repo.CreateVertex(injectionNamed);
                //if the above attack were successful, the vertices table would no longer exist and the read would fail
                var vertices = repo.ReadVertices();
                Assert.AreEqual(1, vertices.Count);
                Assert.IsTrue(VerticesAreSame(injectionNamed, vertices[0]));
            }
            catch (Exception e)
            {
                thrown = e;
            }
            Assert.IsNull(thrown);
        }

        [TestMethod]
        public void CanAccessProductionDatabase()
        {
            var repo = new VertexRepository();
            Exception thrown = null;
            try
            {
                repo.DoesTableExist();
            }
            catch (Exception e)
            {
                thrown = e;
            }
            Assert.IsNull(thrown);
        }
    }
}
