﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Massive.Core.Exceptions;

namespace Massive.Core.Serialization
{
    public static class Serializer
    {
        public static Vertex Deserialize(string filePath)
        {
            using (var reader = XmlReader.Create(filePath, new XmlReaderSettings()))
            {
                var serializer = new System.Xml.Serialization.XmlSerializer(typeof(node));
                var node = (node)serializer.Deserialize(reader);
                //if the node has any adjacent ids, select the ids non-null ones
                var adjacentIds = node.adjacentNodes?.Where(n => !string.IsNullOrEmpty(n.Value)).Select(a => a.Value).ToList() ?? new List<string>();
                //convert the above ids into integer ids
                var adjacentVertices = adjacentIds.Select(adjId => AssertParsed(adjId, node)).ToList();
                var vertex = new Vertex
                {
                    Id = AssertParsed(node.id, node),
                    Label = node.label,
                    Adjacent = adjacentVertices
                };
                if (string.IsNullOrWhiteSpace(vertex.Label)) throw new DeserializationException(node);
                return vertex;
            }
        }

        public static List<Vertex> DeserializeDirectory(string directoryPath)
        {
            var files = Directory.EnumerateFiles(directoryPath);
            var vertices = files.Select(Deserialize).ToList();
            return vertices;
        }

        private static int AssertParsed(string integer, node node)
        {
            int parsed;
            if (!int.TryParse(integer, out parsed)) throw new DeserializationException(node);
            return parsed;
        }
    }
}
