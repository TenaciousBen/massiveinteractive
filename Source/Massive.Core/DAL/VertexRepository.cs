﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Massive.Core.DAL
{
    public class VertexRepository : RepositoryBase<Vertex>
    {
        public override string TableName { get; protected set; } = "vertices";
        
        public VertexRepository()
        {
        }

        public void CreateVertex(Vertex vertex)
        {
            var adjacent = SerializeAdjacents(vertex.Adjacent);
            ExecuteInsert(new Dictionary<string, object>
            {
                { "id", vertex.Id },
                { "label", $"{vertex.Label}" },
                { "adjacent", $"{adjacent}" },
            });
        }

        public List<Vertex> ReadVertices()
        {
            return ExecuteQuery($@"select * from {TableName};", (reader) =>
            {
                var vertex = new Vertex();
                vertex.Id = (int)reader["id"];
                vertex.Label = (string)reader["label"];
                var adjacent = (string)reader["adjacent"];
                vertex.Adjacent = DeserializeAdjacents(adjacent);
                return vertex;
            });
        }

        /// <summary>
        /// Creates the vertices table in the database, overwriting if one already exists.
        /// </summary>
        public override void CreateTable()
        {
            ExecuteNonReader($@"drop table if exists vertices; create table {TableName} (id int, label varchar(512), adjacent varchar(512));");
        }

        private string SerializeAdjacents(List<int> adjacents) => adjacents.ToCsv();
        private List<int> DeserializeAdjacents(string serialized) => serialized.FromCsv(int.Parse); 
    }
}
