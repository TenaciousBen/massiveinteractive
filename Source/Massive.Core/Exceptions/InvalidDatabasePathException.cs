﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Massive.Core.Exceptions
{
    public class InvalidDatabasePathException : Exception
    {
        public InvalidDatabasePathException(string name) : base($"The provided name '{name}' is invalid. It may not contain whitespace or special characters.")
        {
        }
    }
}
