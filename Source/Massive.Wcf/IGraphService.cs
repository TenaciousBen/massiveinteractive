﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Massive.Core;

namespace Massive.Wcf
{
    /// <summary>
    /// A service providing operations to interact with the graph.
    /// </summary>
    [ServiceContract]
    public interface IGraphService
    {
        /// <summary>
        /// Gets all stored vertices as a single graph.
        /// </summary>
        [OperationContract]
        Graph GetGraph();

        /// <summary>
        /// Returns the shortest path between two vertices as list of vertices from
        /// start vertex to end vertex. Returns an empty list if no path exists.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<Vertex> GetShortestPath(PathRequest request);
    }

    /// <summary>
    /// Represents a request to find a path between two vertices.
    /// </summary>
    [DataContract]
    public class PathRequest
    {
        /// <summary>
        /// The id of the vertex at the start of the path.
        /// </summary>
        [DataMember]
        public int StartId { get; set; }
        /// <summary>
        /// The id of the vertex at the end of the path.
        /// </summary>
        [DataMember]
        public int EndId { get; set; }
    }
}
