﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Massive.Wpf.Views
{
    /// <summary>
    /// An auto-resizing canvas that adjusts its required size according to the size of its children.
    /// Adapted from: http://stackoverflow.com/questions/855334/wpf-how-to-make-canvas-auto-resize
    /// </summary>
    public class CanvasAutoSize : Canvas
    {
        protected override System.Windows.Size MeasureOverride(System.Windows.Size constraint)
        {
            base.MeasureOverride(constraint);
            if (InternalChildren.Count == 0) return new Size(0,0);
            var width = base
                .InternalChildren
                .OfType<UIElement>()
                .Max(i => i.DesiredSize.Width + (double)i.GetValue(Canvas.LeftProperty));

            var height = base
                .InternalChildren
                .OfType<UIElement>()
                .Max(i => i.DesiredSize.Height + (double)i.GetValue(Canvas.BottomProperty));

            return new Size(width, height);
        }
    }
}
