﻿using System;
using System.IO;
using System.Linq;
using Massive.Core.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Massive.Tests
{
    [TestClass]
    public class ImportTests : TestBase
    {
        [TestMethod]
        public void CanParseAllNodes()
        {
            //dirty relative path hack, should be replaced by a config file if used in production
            var xmlDir = new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent.GetDirectories().First(d => d.Name == "Resources");
            var xmlFiles = xmlDir.GetFiles();
            var vertices = Serializer.DeserializeDirectory(xmlDir.FullName);
            Assert.AreEqual(vertices.Count, xmlFiles.Length);
            foreach (var xmlFile in xmlFiles)
            {
                var vertice = Serializer.Deserialize(xmlFile.FullName);
                var match = vertices.FirstOrDefault(v => v.Id == vertice.Id 
                    && v.Label == vertice.Label && v.Adjacent.SequenceEqual(vertice.Adjacent));
                Assert.IsNotNull(match);
            }
        }
    }
}
