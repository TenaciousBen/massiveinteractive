﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Massive.Core
{
    public static class GraphExtensions
    {
        public static T MinBy<T>(this IEnumerable<T> source, Func<T, int> selector)
        {
            var eager = source.ToList();
            if (!eager.Any()) return default(T);
            T min = eager.First();
            foreach (var s in eager) if (selector(min) > selector(s)) min = s;
            return min;
        }
        
        public static string ToCsv<T>(this IEnumerable<T> source, Func<T, object> value = null)
        {
            var csv = "";
            var eager = source.ToList();
            for (int i = 0; i < eager.Count; i++)
            {
                csv += value == null ? eager[i].ToString() : value(eager[i]);
                if (i < (eager.Count - 1)) csv += ", ";
            }
            return csv;
        }

        public static List<T> FromCsv<T>(this string csv, Func<string, T> converter)
        {
            return csv.Split(new[] {", "}, StringSplitOptions.RemoveEmptyEntries).Select(converter).ToList();
        }
    }
}
