﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Massive.Wpf.ViewModels;

namespace Massive.Wpf.Views
{
    /// <summary>
    /// A view to layout a graph of vertices and connect vertices with edges.
    /// </summary>
    public partial class GraphPresenter : UserControl
    {
        public Core.Graph ActiveGraph
        {
            get { return (Core.Graph)GetValue(ActiveGraphProperty); }
            set { SetValue(ActiveGraphProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ActiveGraph.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ActiveGraphProperty =
            DependencyProperty.Register("ActiveGraph", typeof(Core.Graph), typeof(GraphPresenter), new PropertyMetadata(null, RemapVertices));

        public static void RemapVertices(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var canvas = dependencyObject.GetChildOfType<Canvas>();
            canvas.Children.Clear();
            var value = dependencyObject.GetValue(ActiveGraphProperty);
            var graph = value as Core.Graph;
            if (graph == null) return;
            MapVertice(graph, canvas);
        }

        public GraphPresenter()
        {
            InitializeComponent();
        }

        public static void MapVertice(Core.Graph graph, Canvas canvas)
        {
            var verticeViewModels = graph.Vertices;
            var verticeQueue = new Queue<Core.Vertex>(verticeViewModels.OrderBy(v => v.Adjacent.Count));
            var views = new Dictionary<int, Vertex>();
            for (var x = 0; verticeQueue.Any(); x++)
            {
                for (var y = 0; y < 3 && verticeQueue.Any(); y++)
                {
                    var xCoord = x * 200;
                    var yCoord = y * 200;
                    var vertice = verticeQueue.Dequeue();
                    var view = VerticeView(vertice, canvas, xCoord, yCoord);
                    views.Add(vertice.Id, view);
                    canvas.Children.Add(view);
                }
            }
            foreach (var vertice in views.Values)
            {
                if (vertice.SourceVertexViewModel.Adjacent.Count == 0) continue;
                foreach (var adj in vertice.SourceVertexViewModel.Adjacent)
                {
                    var neighbour = views[adj];
                    var edge = Edge(vertice, neighbour);
                    canvas.Children.Add(edge);
                }
            }
        }

        public static Vertex VerticeView(Core.Vertex vertexViewModel, Canvas canvas, int x, int y)
        {
            var verticeView = new Vertex();
            verticeView.Label = vertexViewModel.Label;
            verticeView.ContainerCanvas = canvas;
            Canvas.SetLeft(verticeView, x);
            Canvas.SetBottom(verticeView, y);
            verticeView.Id = vertexViewModel.Id;
            verticeView.Pathed = vertexViewModel.Path;
            verticeView.Name = $"{vertexViewModel.Label}{vertexViewModel.Id}";
            verticeView.SourceVertexViewModel = vertexViewModel;
            verticeView.Selected = vertexViewModel.IsSelected;
            return verticeView;
        }

        public static Connector Edge(Vertex a, Vertex b)
        {
            var isPath = a.Pathed && b.Pathed;
            var edge = new Connector(isPath);
            edge.SetBinding(Connector.SourceProperty, new Binding("AnchorPoint")
            {
                Source = a
            });
            edge.SetBinding(Connector.DestinationProperty, new Binding("AnchorPoint")
            {
                Source = b
            });
            return edge;
        }
    }
}
