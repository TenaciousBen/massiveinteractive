﻿using System;
using System.Collections.Generic;
using System.Linq;
using Massive.Core;
using Massive.Core.DAL;
using Massive.Tests.Mocks;
using Massive.Wcf;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IGraphService = Massive.Wpf.GraphService.IGraphService;
using PathRequest = Massive.Wpf.GraphService.PathRequest;

namespace Massive.Tests
{
    [TestClass]
    public class WcfTests : TestBase
    {
        public VertexRepository Repository { get; set; } = new TestVertexRepository();

        public WcfTests()
        {
            Repository.CreateTable();
        }

        [TestMethod]
        public void CanReadGraph()
        {
            var vertices = CreateGraph();
            Wcf.IGraphService client = new Wcf.GraphService(Repository);
            var graph = client.GetGraph();
            Assert.AreEqual(4, graph.Vertices.Count);
            foreach (var vertice in vertices) Assert.AreEqual(1, graph.Vertices.Count(v => VerticesAreSame(vertice, v)));
        }

        [TestMethod]
        public void CanDetermineShortestPath()
        {
            var vertices = CreateGraph();
            Wcf.IGraphService client = new Wcf.GraphService(Repository);
            var start = vertices.First(v => v.Id == 1);
            var end = vertices.First(v => v.Id == 4);
            var path = client.GetShortestPath(new Wcf.PathRequest {StartId = 1, EndId = 4});
            Assert.IsTrue(VerticesAreSame(path.First(), start));
            Assert.IsTrue(VerticesAreSame(path.Last(), end));
        }

        private List<Vertex> CreateGraph()
        {
            var start = CreateVertex();
            var deadEnd = CreateVertex(start);
            var middle = CreateVertex(start);
            var finish = CreateVertex(middle);
            Repository.CreateVertex(start);
            Repository.CreateVertex(deadEnd);
            Repository.CreateVertex(middle);
            Repository.CreateVertex(finish);
            return new List<Vertex> {start, deadEnd, middle, finish};
        }
    }
}
