﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Massive.Core;
using Massive.Core.DAL;
using Massive.Core.Serialization;

namespace Massive.Importer
{
    class Program
    {
        static void Main(string[] args)
        {
            var quit = true;
            do
            {
                try
                {
                    Console.WriteLine("Thanks for considering me for this position, please read the readme.md in the root of the source directory");
                    Console.WriteLine("Feel free to get in touch at ben@apirion.com if you run into any troubles");
                    var directory = PromptDirectory("Please enter the directory containing the node XML files:");
                    var vertices = Serializer.DeserializeDirectory(directory.FullName);
                    LinkAllVertices(vertices);
                    var repo = new VertexRepository();
                    repo.CreateTable();
                    foreach (var vertex in vertices) repo.CreateVertex(vertex);
                    quit = PromptYesNo("Import complete. Quit?");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Encountered the following exception: {e.GetType().Name} - {e.Message}");
                    Console.WriteLine($"Please check the readme.md and ensure that the application has permissions over the db directory and the xml directory");
                    Console.WriteLine($"Feel free to get in touch at ben@apirion.com if the issue cannot be resolved");
                    Console.ReadLine();
                }
            } while (!quit);
        }

        static T Prompt<T>(string prompt, Func<string, T> converter, Func<T, bool> allowContinue)
        {
            T parsed;
            do
            {
                Console.WriteLine(prompt);
                try
                {
                    parsed = converter(Console.ReadLine());
                }
                catch (Exception e)
                {
                    parsed = default(T);
                }
            } while (!allowContinue(parsed));
            return parsed;
        }

        static bool PromptYesNo(string prompt)
        {
            // ReSharper disable once PossibleInvalidOperationException
            // the boolean is forced to have a value in the third parameter
            return Prompt($"{prompt} (Y/N)", s => s.ToBooleanFromYesNo(), b => b.HasValue).Value;
        }

        static DirectoryInfo PromptDirectory(string prompt)
        {
            return Prompt($"{prompt}", s => new DirectoryInfo(s), d => d != null && d.Exists);
        }

        static void LinkAllVertices(List<Vertex> vertices)
        {
            foreach (var vertice in vertices)
            {
                foreach (var adjacent in vertice.Adjacent)
                {
                    var matched = vertices.FirstOrDefault(v => v.Id == adjacent);
                    if (matched == null || matched.Adjacent.Contains(vertice.Id)) continue;
                    matched.Adjacent.Add(vertice.Id);
                }
            }
        }
    }
}
