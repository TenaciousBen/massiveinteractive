﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Massive.Core;
using Massive.Wpf.Annotations;
using Massive.Wpf.GraphService;

namespace Massive.Wpf.ViewModels
{
    public class GraphViewModel : INotifyPropertyChanged
    {
        private Graph _activeGraph;
        public Graph ActiveGraph
        {
            get { return _activeGraph; }
            set
            {
                _activeGraph = value;
                OnPropertyChanged();
            }
        }
        public BasicCommand LoadGraph { get; set; }
        public ValidatedCommand<Graph> FindPath { get; set; }

        public GraphViewModel()
        {
            LoadGraph = new BasicCommand(LoadGraphAction);
            FindPath = new ValidatedCommand<Graph>(FindPathAction, graph => graph != null && graph.Vertices.Count(v => v.IsSelected) == 2);
        }

        public async void LoadGraphAction()
        {
            using (var graphClient = new GraphService.GraphServiceClient())
            {
                ActiveGraph = await graphClient.GetGraphAsync();
                ActiveGraph.PropertyChanged += (sender, e) => FindPath.RaiseCanExecuteChanged();
                ActiveGraph.StartWatchingVertices();
            }
        }

        public async void FindPathAction()
        {
            var selectedVertices = ActiveGraph.Vertices.Where(v => v.IsSelected).ToList();
            if (selectedVertices.Count != 2) throw new Exception($"Expected two vertices to be selected, got {selectedVertices.Count}");
            using (var graphClient = new GraphService.GraphServiceClient())
            {
                var path = await graphClient.GetShortestPathAsync(new PathRequest { StartId = selectedVertices[0].Id, EndId = selectedVertices[1].Id });
                ActiveGraph.SetActivePath(path.ToList());
                var graph = ActiveGraph;
                ActiveGraph = null;
                ActiveGraph = graph;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
