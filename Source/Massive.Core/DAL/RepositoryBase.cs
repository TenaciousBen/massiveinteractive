﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Massive.Core.Exceptions;

namespace Massive.Core.DAL
{
    public abstract class RepositoryBase<T>
    {
        public abstract string TableName { get; protected set; }

        /// <summary>
        /// The connection string for the database.
        /// </summary>
        protected string ConnectionString { get; private set; } = @"URI=file:C:\Windows\Temp\massive.db";

        /// <summary>
        /// Creates the table required by the repository. The derived class is responsible for checking whether the
        /// table already exists.
        /// </summary>
        public abstract void CreateTable();

        /// <summary>
        /// Returns true if the table used by the derived class exists in the database.
        /// </summary>
        public bool DoesTableExist()
        {
            var matchingTables = ExecuteQuery<string>($"SELECT name FROM sqlite_master WHERE type='table' AND name='{TableName}';", r => (string)r["name"]);
            return matchingTables.Count > 0 && matchingTables[0] == TableName;
        }

        /// <summary>
        /// Executes a command with no response e.g. deletion.
        /// </summary>
        protected void ExecuteNonReader(string sql)
        {
            using (var conn = OpenConnection())
            {
                using (var query = new SQLiteCommand(sql, conn))
                {
                    query.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Executes an insert as a prepared statement, thereby rejecting SQL injection attacks
        /// </summary>
        protected void ExecuteInsert(Dictionary<string, object> columnValues)
        {
            var statement = new StringBuilder($"insert into {TableName} (");
            statement.Append(columnValues.Keys.ToCsv());
            statement.Append(") values (");
            statement.Append($"{columnValues.Keys.Select((k, i) => $"@p{i + 1}").ToCsv()})");
            using (var connection = OpenConnection())
            {
                using (var command = new SQLiteCommand(statement.ToString(), connection))
                {
                    for (var i = 0; i < columnValues.Values.Count; i++)
                    {
                        var value = columnValues.Values.ElementAt(i);
                        command.Parameters.AddWithValue($"@p{i + 1}", value);
                    }
                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Executes a reader e.g. a select, delegating the initialization of objects from rows to the caller.
        /// Allows reading of any type of row.
        /// </summary>
        protected List<TRow> ExecuteQuery<TRow>(string sql, Func<SQLiteDataReader, TRow> rowReader)
        {
            using (var conn = OpenConnection())
            {
                using (var query = new SQLiteCommand(sql, conn))
                {
                    var reader = query.ExecuteReader();
                    var items = new List<TRow>();
                    while (reader.Read())
                    {
                        items.Add(rowReader(reader));
                    }
                    return items;
                }
            }
        }

        /// <summary>
        /// Executes a reader e.g. a select, delegating the initialization of objects from rows to the caller.
        /// </summary>
        protected List<T> ExecuteQuery(string sql, Func<SQLiteDataReader, T> rowReader) => ExecuteQuery<T>(sql, rowReader);

        /// <summary>
        /// Creates and opens a new connection to the specified SQLite database.
        /// </summary>
        protected SQLiteConnection OpenConnection()
        {
            var conn = new SQLiteConnection(ConnectionString);
            return conn.OpenAndReturn();
        }

        /// <summary>
        /// Changes the active database to the parameter named SQLite database. Expects
        /// the name portion only, and will throw an exception if the name contains whitespace
        /// or anything other than letters.
        /// </summary>
        protected void ChangeDatabaseFile(string filename)
        {
            if (string.IsNullOrEmpty(filename) || filename.Any(c => !char.IsLetter(c))) throw new InvalidDatabasePathException(filename);
            ConnectionString = $@"URI=file:C:\Windows\Temp\{System.IO.Path.ChangeExtension(filename, ".db")}";
        }
    }
}
