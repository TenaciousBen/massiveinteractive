﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Massive.Wpf.Views
{
    /// <summary>
    /// A vertex which provides anchor points for the <see cref="Connector"/>, interaction
    /// for the user, replication of Selected status through to the model and highlighting for
    /// pathed vertices.
    /// </summary>
    public partial class Vertex : UserControl
    {
        public Core.Vertex SourceVertexViewModel
        {
            get { return (Core.Vertex)GetValue(SourceVerticeViewModelProperty); }
            set { SetValue(SourceVerticeViewModelProperty, value); }
        }

        public static readonly DependencyProperty SourceVerticeViewModelProperty =
            DependencyProperty.Register("SourceVerticeViewModel", typeof(Core.Vertex), typeof(Vertex), new PropertyMetadata(null));

        public Point AnchorPoint
        {
            get { return (Point)GetValue(AnchorPointProperty); }
            set { SetValue(AnchorPointProperty, value); }
        }

        public static readonly DependencyProperty AnchorPointProperty =
            DependencyProperty.Register("AnchorPoint", typeof(Point), typeof(Vertex),
                new FrameworkPropertyMetadata(new Point(0, 0), FrameworkPropertyMetadataOptions.AffectsMeasure));

        public Canvas ContainerCanvas
        {
            get { return (Canvas)GetValue(ContainerCanvasProperty); }
            set { SetValue(ContainerCanvasProperty, value); }
        }

        public static readonly DependencyProperty ContainerCanvasProperty =
            DependencyProperty.Register("ContainerCanvas", typeof(Canvas), typeof(Vertex), new PropertyMetadata(null));

        public bool Selected
        {
            get { return (bool)GetValue(SelectedProperty); }
            set { SetValue(SelectedProperty, value); }
        }

        public static readonly DependencyProperty SelectedProperty =
            DependencyProperty.Register("Selected", typeof(bool), typeof(Vertex), new PropertyMetadata(false, OnSelected));

        public int Id
        {
            get { return (int)GetValue(IdProperty); }
            set { SetValue(IdProperty, value); }
        }

        public static readonly DependencyProperty IdProperty =
            DependencyProperty.Register("Id", typeof(int), typeof(Vertex), new PropertyMetadata(0));

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(Vertex), new PropertyMetadata("Default"));

        public bool Pathed
        {
            get { return (bool)GetValue(PathedProperty); }
            set { SetValue(PathedProperty, value); }
        }

        public static readonly DependencyProperty PathedProperty =
            DependencyProperty.Register("Pathed", typeof(bool), typeof(Vertex), new PropertyMetadata(false));

        public Vertex()
        {
            InitializeComponent();
            MouseUp += (sender, args) => SetValue(SelectedProperty, !Selected);
            this.LayoutUpdated += OnLayoutUpdated;
        }

        private void OnLayoutUpdated(object sender, EventArgs e)
        {
            //it can take a little bit for the canvas to reorganise its descendents, so ensure the canvas is this control's parent
            //and that we're not in design mode
            if (ContainerCanvas == null || !ContainerCanvas.IsAncestorOf(this) || DesignerProperties.GetIsInDesignMode(this)) return;
            //stay in front of connectors
            Panel.SetZIndex(this, 5);
            var size = RenderSize;
            var center = new Point(size.Width / 2, size.Height / 2);
            //set the anchor point for connectors to the center of the visual
            AnchorPoint = TransformToVisual(ContainerCanvas).Transform(center);
        }

        private static void OnSelected(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            //propagate selection changes through to the model
            var vertex = (Core.Vertex)dependencyObject.GetValue(SourceVerticeViewModelProperty);
            var isSelected = (bool)dependencyObject.GetValue(SelectedProperty);
            vertex.IsSelected = isSelected;
        }
        public override string ToString() => $"{Id} {Label} - IsSelected:{Selected} - Path:{Pathed}";
    }
}
